json.array!(@customers) do |customer|
  json.extract! customer, :id, :firstname, :secondname, :companyName, :phoneNumber
  json.url customer_url(customer, format: :json)
end

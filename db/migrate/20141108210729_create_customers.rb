class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :firstname
      t.string :secondname
      t.string :companyName
      t.string :phoneNumber

      t.timestamps
    end
  end
end

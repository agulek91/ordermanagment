class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :title
      t.text :content
      t.references :orderStatus, index: true
      t.references :customer, index: true

      t.timestamps
    end
  end
end

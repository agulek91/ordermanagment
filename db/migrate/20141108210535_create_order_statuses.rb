class CreateOrderStatuses < ActiveRecord::Migration
  def change
    create_table :order_statuses do |t|
      t.string :status
      t.text :content
      t.references :user, index: true
      t.date :updateDate

      t.timestamps
    end
  end
end
